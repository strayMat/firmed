import os
import pathlib

LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")

ROOT_DIR = pathlib.Path(
    os.getenv("ROOT_DIR", os.path.dirname(os.path.abspath(__file__)) + "/..")
)
DIR2DATA = ROOT_DIR / "data"


DIR2LLMS = pathlib.Path(os.getenv("DIR2LLMS", "/home/has/llms"))
