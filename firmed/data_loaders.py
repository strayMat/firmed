import requests
import zipfile
import io
from firmed.constants import DIR2DATA
from tqdm import tqdm
import logging

logger = logging.getLogger(__name__)


# taken from : https://gist.github.com/yanqd0/c13ed29e29432e3cf3e7c38467f42f51
def download(url: str, fname: str, chunk_size=1024):
    resp = requests.get(url, stream=True)
    total = int(resp.headers.get("content-length", 0))
    with open(fname, "wb") as file, tqdm(
        desc=fname,
        total=total,
        unit="iB",
        unit_scale=True,
        unit_divisor=1024,
    ) as bar:
        for data in resp.iter_content(chunk_size=chunk_size):
            size = file.write(data)
            bar.update(size)


def download_has_publications():
    dir2publications = DIR2DATA / "has_publications"
    dir2publications.mkdir(parents=True, exist_ok=True)
    # raw texts
    url = "https://www.data.gouv.fr/fr/datasets/r/75369b1c-735e-4c31-8080-42f1396a6390"
    dir2texts = dir2publications / "texts"

    if not dir2texts.exists():
        logger.info(f"Downloading publication texts from {url} to {dir2texts}")
        dir2texts.mkdir(parents=True, exist_ok=True)
        response = requests.get(url)
        response.raise_for_status()  # Check if the request was successful
        z = zipfile.ZipFile(io.BytesIO(response.content))
        z.extractall(path=dir2texts)

    # metadata
    metadata_url = (
        "https://www.data.gouv.fr/fr/datasets/r/10795a76-4b33-4e8d-b142-43485cfa85bf"
    )
    dir2metadata = dir2publications / "metadata"
    if not dir2metadata.exists():
        logger.info(
            f"Downloading publication metadata from {metadata_url} to {dir2metadata}"
        )
        dir2metadata.mkdir(parents=True, exist_ok=True)
        download(metadata_url, str(dir2metadata / "metadata.zip"))
        with zipfile.ZipFile(dir2metadata / "metadata.zip", "r") as fp:
            fp.extractall(dir2metadata)
