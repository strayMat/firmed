# Firmed

**Documentation**: [https://strayMat.pages.has-sante.fr/strayMat/firmed](https://strayMat.pages.has-sante.fr/strayMat/firmed)

**Source code**: [https://gitlab.com/strayMat/firmed](https://gitlab.com/strayMat/firmed)

---

## Overview


French Information Retrieval from MEdical Documents is a week-end project to explore the use of modern NLP tools to extract information from french medical texts or . It is a pretext to experiment again with NLP tools and information retrieval from documents. 

### Goals

I will first experiment with information retrieval endpoints that I encounter in my professional life : 

- Extracting information from medical reports : dates, diagnosis, weights
- Tagging information in publications. 

### Tools 

The primary goal is to compare rule based librairies (mainly [eds-nlp](https://aphp.github.io/edsnlp/latest/)) to LLMs approaches such as : [langchain](https://python.langchain.com/docs/use_cases/extraction) combined with open LLMs such as Mistral 7B, [DrBert](https://drbert.univ-avignon.fr/).

### Data

Since clinical texts are private, I will use public datasets to experiment with the tools, namely : 

- [Publications of the Haute Autorité de Santé](https://www.data.gouv.fr/fr/datasets/metadonnees-des-publications-de-la-has-1/)
- Maybe the [Nachos corpus]() used for 


### Long term goals 

Long term goals and questions comprise : 

- How to conduct structured information retrieval from healtcare literature, eg., with [Grade](https://training.cochrane.org/handbook/current/chapter-14) ? 
- Leverage annotation in clinical texts to build proxies for physiological signals with claims data 
