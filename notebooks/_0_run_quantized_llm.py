# %%
from firmed.data_loaders import download_has_publications
from firmed.constants import DIR2LLMS

from llama_cpp import Llama
## to download directly from the web
# from huggingface_hub import hf_hub_download
# model_path = hf_hub_download(model_name, filename=model_file)
# %%
# df = download_has_publications()

model_name = "mistral-7b-instruct-v0.2.Q5_K_M.gguf"  #
## Instantiate model from downloaded file
llm = Llama(
    model_path=str(DIR2LLMS / model_name),
    n_ctx=128,  # Context length to use
    # n_threads=32,  # Number of CPU threads to use : if set, make the inference super slow (even if I have 16 threads)
    n_gpu_layers=0,  # Number of model layers to offload to GPU
)

# ## Generation kwargs
# generation_kwargs = {
#     "max_tokens": 20,
#     "stop": ["</s>"],
#     "echo": False,  # Echo the prompt in the output
#     "top_k": 1,  # This is essentially greedy decoding, since the model will always return the highest-probability token. Set this value > 1 for sampling decoding
# }
# %%
prompt = "The age of the universe is "
res = llm(prompt, stop=["</s>"], max_tokens=52)  # Res is a dictionary
## Unpack and the generated text from the LLM response dictionary and print it
print(res["choices"][0]["text"])
# res is short for result
